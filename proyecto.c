#include <stdio.h>               
#include <string.h>  
#include <stdlib.h> 

float monto = 1000; /*Monto incial de $1000*/

/*Funcion que permitira consultar la cantidad de dinero en la cuenta*/
float consultar(){
	if(monto >= 0) {
		return monto;
	} 
	return 0;
}

/*Funcion que permitira retirar dinero de la cuenta*/
float retirar(float valorRetiro){	

	//El valor a retirar debe ser menor o igual al monto de la cuenta, y un valor positivo
	if((monto - valorRetiro) >= 0 && valorRetiro >= 0) { 
		monto = monto - valorRetiro;
		printf("\nSu retiro fue realizado con exito.\n");		
		return monto;
	} else if (valorRetiro <= 0){ //Si el valor a retirar es menor a 0 se imprimira lo siguiente
		printf("NOTA: El valor a ingresar tiene que ser mayor a 0.\n");
		return 0;
	} else { //Si el valor a retirar es mayor al monto se imprimira lo siguiente
		printf("NOTA: Su retiro no puede ser efectuado. Cantidad ingresada mayor a su saldo.\n");
	}
	return 0;
}

float depositar(float valorDeposito){
	//El valor a depositar debe ser positivo
	if(valorDeposito >= 0) {
		monto = monto + valorDeposito;
		printf("\nSu deposito fue realizado con exito.\n");		
		return monto;
	} else { //Si el valor a depositar es menor a 0 se imprimira lo siguiente
		printf("NOTA: El valor a ingresar tiene que ser mayor a 0.\n");
	}
	
	return 0;
}

int main(){

	int opcion = 0; //Opcion del menu a ingresar
	char continuar = 's'; //Respuesta a la pregunta de continuar el programa
	float valor = 0.0; //Valor a ingresar por teclado (sea para retiro o deposito)

	printf("*** BIENVENIDO/A ***\n");

	//El menu aparecera cada vez que se responda a la pregunta con la letra 's'
	while(strcmp(&continuar, "s") == 0){
	
		/*MENU DE OPCIONES*/
		printf("\n1. Consulta de saldos\n");
		printf("2. Realizar Depósito\n");
		printf("3. Realizar Retiro\n\n");

		//Ingreso de la opcion del menu (la cual debe ser un numero entero)
		printf("Elija la opcion a realizar: ");
		scanf("%d", &opcion);
	
		//Si no se ingreso una opcion del 1 al 3 del menu, entonces se pedira nuevamente el ingreso de esta
		while(opcion != 1 && opcion != 2 && opcion != 3){
			printf("Elija la opcion a realizar: ");
			scanf("%d", &opcion);
		}

		//Si se eligio la opcion 1 del menu, se procedera a llamar a la funcion consultar()
		if(opcion == 1){
			printf("\nSu saldo actual es: %.2f \n", consultar());
		} 

		//Si se eligio la opcion 2 del menu, se procedera a llamar a la funcion depositar()
		if(opcion == 2){
			printf("\nIngrese la cantidad a depositar: ");
			scanf("%f", &valor);
			//Si la funcion nos retorna un 0 significa que el valor ingresado fue negativo, por lo tanto se
			//continuara solicitando el ingreso de la cantidad a depositar adecuada
			while(depositar(valor) == 0){
				printf("\nIngrese la cantidad a depositar: ");
				scanf("%f", &valor);
			}
		} 
	
		//Si se eligio la opcion 3 del menu, se procedera a llamar a la funcion retirar()
		if(opcion == 3){
			printf("\nIngrese la cantidad a retirar: ");
			scanf("%f", &valor);
			//Si la funcion nos retorna un 0 significa que el valor ingresado fue negativo o mayor al monto, por lo tanto se
			//continuara solicitando el ingreso de la cantidad a retirar adecuada
			while(retirar(valor) == 0){
				printf("\nIngrese la cantidad a retirar: ");
				scanf("%f", &valor);
			}
			
		}

		//Esta pregunta, con su respectiva respuesta, define si el programa continua o se termina
		printf("\n¿Desea continuar? [s/n]: ");
		scanf("%s", &continuar);

		//Si la opcion ingresada no fue una 's' o una 'n' se continuara solicitando el ingreso de la opcion correcta
		while(strcmp(&continuar, "s") != 0 && strcmp(&continuar, "n") != 0){
			printf("\n¿Desea continuar? [s/n]: ");
			scanf("%s", &continuar);
		}
		
		//Si la opcion ingresada fue una 'n', entonces el programa finaliza
		if(strcmp(&continuar, "n") == 0){
			printf("\n\n*** GRACIAS ***\n\n");
			return 0;		
		}

	}

	return 0;

}
