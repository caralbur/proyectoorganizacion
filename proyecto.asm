.data
	#Declaracion de diferentes mensajes a utilizar a lo largo del programa

	mensaje: .asciiz "*** BIENVENIDO/S ***"
	op1: .asciiz "\n\n1. Consulta de saldos\n"
	op2: .asciiz "2. Realizar Dep�sito\n"
	op3: .asciiz "3. Realizar Retiro\n"
	
	ingreso: .asciiz "\nElija la opcion a realizar: "
		
	consulta: .asciiz "\nSu saldo actual es: "
	
	ingresoDep: .asciiz "\nIngrese la cantidad a depositar: "
	depGood: .asciiz "Su deposito fue realizado con exito."
	
	ingresoRet: .asciiz "\nIngrese la cantidad a retirar: "
	ingBad: .asciiz "NOTA: El valor a ingresar tiene que ser mayor a 0.\n"
	retGood: .asciiz "Su retiro fue realizado con exito."
	retBad: .asciiz "NOTA: Su retiro no puede ser efectuado. Cantidad ingresada mayor a su saldo.\n"
	
	pregunta: .asciiz "\n\n�Desea continuar? [s/n]: "
	
	salida: .asciiz "\n\n*** GRACIAS ***\n"
	
	monto: .float 1000.0				#Declaracion del valor del monto inicial
	zeroFloat: .float 0.0				#Variable float con 0 debido a que no acepta $zero en las operaciones float
	
	.align 1
	
.text
	main:	
		lwc1 $f0, monto				#Cargar en $f0 el monto incial
		lwc1 $f1, zeroFloat			#Cargar en $f1 el valor de cero
		
		#Imprimir la cadena de bienvenida
			
		li $v0, 4				#Llamada al sistema para imprimir un String		
		la $a0, mensaje				#Direccion del String a imprimir
		syscall					#Llamada a imprimir
		
		jal mostrarMensaje			#Saltar a la ejecucion del programa
	
	mostrarMensaje: 
	
		#Imprimir las opciones del menu
				
		li $v0, 4				#Llamada al sistema para imprimir un String
		
		sub $a0, $a0, 0				#Quitar el valor anterior a $a0
		la $a0, op1				#Direccion del String a imprimir
		syscall 				#Llamada a imprimir
		
		sub $a0, $a0, 0				#Quitar el valor anterior a $a0
		la $a0, op2				#Direccion del String a imprimir
		syscall					#Llamada a imprimir
		
		sub $a0, $a0, 0				#Quitar el valor anterior a $a0
		la $a0, op3				#Direccion del String a imprimir
		syscall					#Llamada a imprimir
		
		INGRESO:				#Funcion para retomar el programa desde la eleccion de opcion del menu
		
			add.s $f12, $f0, $f1		#Agregar el monto incial al principio a f12 (sumandole el valor de cero)
			
			#Imprimir el mensaje de ingreso de la opcion del menu
		
			li $v0, 4			#Llamada al sistema para imprimir un String		
			
			sub $a0, $a0, 0			#Quitar el valor anterior a $a0
			la $a0, ingreso			#Direccion del String a imprimir
			syscall 			#Llamada a imprimir
		
			sub $a0, $a0, 0			#Quitar el valor anterior a $a0
			li $v0, 5			#Llamada al sistema para leer una entero
			syscall				#Llamada a ingreso de opcion del menu
				
			move $t0, $v0			#Opcion del menu escogida
			addi $t1, $zero, 1		#Opcion 1 del menu
			addi $t2, $zero, 2		#Opcion 2 del menu
			addi $t3, $zero, 3		#Opcion 3 del menu	
			
			#Comparar que opcion del menu fue escogida para realizar la accion respectiva	
			
			#Primer IF del menu			
						
			beq $t0,$t1,L1			#Dirigirse a la funcion a realizar si la condicion se cumple (L1)
			jal IF2				#Salto al siguiente IF (funciona como un ELSE IF)
			
		L1:
			li $v0, 4			#Llamada al sistema para imprimir un String
			sub $a0, $a0, 0			#Quitar el valor anterior a $a0
			la $a0, consulta		#Direccion del String a imprimir
			syscall 			#Llamada a imprimir
							
			li $v0, 2			#Llamada al sistema para imprimir un float
			syscall				#Llamada a imprimir
			
			jal PRE				#Salto a pregunta que se realiza para continuar o terminar el programa
			
			#Segundo IF del menu
			
		IF2: 
			beq $t0,$t2,L2			#Dirigirse a la funcion a realizar si la condicion se cumple (L2)
			jal IF3				#Salto al siguiente IF (funciona como un ELSE IF)
		L2:	
			li $v0, 4			#Llamada al sistema para imprimir un String
			sub $a0, $a0, 0			#Quitar el valor anterior a $a0
			la $a0, ingresoDep		#Direccion del String a imprimir
			syscall 			#Llamada a imprimir
						
			li $v0, 6			#Llamada al sistema para leer un float
			syscall				#Llamada a ingreso del valor a depositar		
			
			lwc1 $f1,zeroFloat		#Cargar a f1 el valor de 0
			c.lt.s $f0 $f1			#Comparar si el valor ingresado es menor a 0
			bc1f SUMA			#Si la condicion fue falsa, se ira a la funcion SUMA y se hara el deposito
			
			#Si la condicion fue verdadera
			li $v0, 4			#Llamada al sistema para imprimir un String
			sub $a0, $a0, 0			#Quitar el valor anterior a $a0
			la $a0,  ingBad			#Direccion del String a imprimir
			syscall				#Llamada a imprimir
				
			jal L2				#Se volvera a preguntar por el valor a depositar
			
		SUMA:
			add.s $f0, $f0, $f12		#Suma el monto inicial con el valor ingresado
			
			li $v0, 4			#Llamada al sistema para imprimir un String
			sub $a0, $a0, 0			#Quitar el valor anterior a $a0
			la $a0, depGood			#Direccion del String a imprimir
			syscall				#Llamada a imprimir
			
			jal PRE				#Salto a pregunta que se realiza para continuar o terminar el programa
			
			#Tercer IF del menu
			
		IF3: 
			beq $t0,$t3,L3			#Dirigirse a la funcion a realizar si la condicion se cumple (L3)
			jal INGRESO			#Salto a eleccion de opciones (si no se eligio la opcion correcta del menu)
		L3:				
			li $v0, 4			#Llamada al sistema para imprimir un String
			sub $a0, $a0, 0			#Quitar el valor anterior a $a0
			la $a0, ingresoRet		#Direccion del String a imprimir
			syscall				#Llamada a imprimir
			
			li $v0, 6			#Llamada al sistema para leer un float
			syscall				#Llamada a ingreso del valor a retirar			
						
			c.lt.s $f0 $f12			#Comparar si el monto total es menor al valor ingresado
			bc1t COMP  			#Si la condicion fue verdadera, se ira a la funcion COMP
			
			#Si la condicion fue falsa
			li $v0, 4			#Llamada al sistema para imprimir un String
			sub $a0, $a0, 0			#Quitar el valor anterior a $a0
			la $a0, retBad			#Direccion del String a imprimir
			syscall				#Llamada a imprimir
				
			jal L3				#Se volvera a preguntar por el valor a retirar
			
		COMP:	
			lwc1 $f1,zeroFloat		#Cargar a f1 el valor de 0
			c.lt.s $f0 $f1			#Comparar si el valor ingresado es menor a 0
			bc1f RESTA			#Si la condicion fue falsa, se ira a la funcion RESTA y se hara el retiro
			
			#Si la condicion fue verdadera
			li $v0, 4			#Llamada al sistema para imprimir un String
			sub $a0, $a0, 0			#Quitar el valor anterior a $a0
			la $a0, ingBad			#Direccion del String a imprimir
			syscall				#Llamada a imprimir
			
			jal L3				#Se volvera a preguntar por el valor a retirar
			
		RESTA:
			sub.s $f0, $f12, $f0		#Resta el monto inicial con el valor ingresado	
			
			li $v0, 4			#Llamada al sistema para imprimir un String
			sub $a0, $a0, 0			#Quitar el valor anterior a $a0
			la $a0, retGood			#Direccion del String a imprimir
			syscall				#Llamada a imprimir
			
			jal PRE				#Salto a pregunta que se realiza para continuar o terminar el programa
			
			#Pregunta que se realiza para continuar o terminar el programa
		PRE:
			li $v0, 4			#Llamada al sistema para imprimir un String		
			sub $a0, $a0, 0			#Quitar el valor anterior a $a0
			la $a0, pregunta		#Direccion del String a imprimir
			syscall				#Llamada a imprimir

			sub $a0, $a0, 0			#Quitar el valor anterior a $a0
			li $v0, 12			#Llamada al sistema para leer un char (no strings para poder 
							#hacer la comparacion con su equivalente en numero)
			syscall				#Llamada para el ingreso del caracter
			
			move $t1, $v0			#Opcion escogida de la pregunta 
			
			#Esto se vuelve a agregar para que se permita el dar enter despues de ingresar el caracter (sino salta
			#directamente a lo que debe realizar)
			sub $a0, $a0, 0			#Quitar el valor anterior a $a0
			li $v0, 12			#Llamada al sistema para leer un char
			syscall				#Llamada para el ingreso del caracter
			
        		addi $t2, $zero, 115		#115 es el valor del caracter 's'
        		addi $t3, $zero, 110		#110 es el valor del caracter 'n'       	      	
        					
        		#Primer IF de la pregunta 
			beq $t1, $t2, mostrarMensaje	#Si el caracter ingresado es 's', se vuelve a mostrar el menu del programa
			jal NOTSAME			#Si lo anterior no se cumple, procede a dirigirse a la funcion NOTSAME 
			
			#Segundo IF de la pregunta 
		NOTSAME: 			
			beq $t1, $t3, TERM		#Si el caracter ingresado es 'n', procede a dirigirse a la funcion TERM
			jal PRE				#Si no eligen la 's' o la 'n', se repite la pregunta
			
			#Terminar el programa
		TERM:
			li $v0, 4			#Llamada al sistema para imprimir un String
			la $a0, salida			#Direccion del string a imprimir	
			syscall				#Llamada a imprimir
		
			li $v0, 10 			#Llamada al sistema para termine
			syscall	
			
			
